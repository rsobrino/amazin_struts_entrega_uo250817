package com.miw.business;

import java.util.List;

import com.miw.infrastructure.Factories;
import com.miw.model.User;

public class UserDataServiceHelper {

	public List<User> getUsers() throws Exception {
		return (Factories.dataServices.getUserDataService()).getUsers();
	}

	public User getUserByUsername(String username) throws Exception {
		return (Factories.dataServices.getUserDataService()).getUserByUsername(username);
	}

	public User newUser(User user) throws Exception {
		return (Factories.dataServices.getUserDataService()).newUser(user);
	}
}
