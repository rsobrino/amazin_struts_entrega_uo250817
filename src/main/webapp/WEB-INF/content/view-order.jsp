<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html >
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<head>
<title><s:text name="title" /></title>
<link rel="stylesheet" href="css/style.css" />
</head>
<body>
	<header>
		<h1 class="header">
			<s:text name="index.h1" />
		</h1>
		<h2 class="centered">
			<s:text name="index.h2.part1" />
			<em><s:text name="index.h2.part2" /></em>
			<s:text name="index.h2.part3" />
		</h2>
	</header>
	<nav>
		<ul>
			<li><a href="#"><s:text name="index.nav.list1" /></a></li>
			<li><a href="http://miw.uniovi.es"><s:text
						name="index.nav.list2" /></a></li>
			<li><a href="mailto:dd@email.com"><s:text
						name="index.nav.list3" /></a></li>
		</ul>
	</nav>
	<section>
		<article>
			<s:if test="%{#request.orderList.isEmpty()}">

				<p>
					<s:text name="order.noorders" />
				</p>
			</s:if>
			<s:else>
				<table>
					<caption>
						<s:text name="order.orders" />
					</caption>
					<thead>
						<tr>
							<th><s:text name="order.code" /></th>
							<th><s:text name="order.payment" /></th>
							<th><s:text name="order.address" /></th>
							<th><s:text name="order.username" /></th>
							<th><s:text name="order.numerolibros" /></th>
							<th><s:text name="order.total" /></th>
							<th><s:text name="order.fecha" /></th>
						</tr>
					</thead>
					<tbody>
						<s:iterator value="%{#request.orderList}">
							<tr>
								<td><s:property value="code" /></td>
								<td><s:property value="payment" /></td>
								<td><s:property value="address" /></td>
								<td><s:property value="username" /></td>
								<td><s:property value="numeroLibros" /></td>
								<td><s:property value="total" /> &euro;</td>
								<td><s:date name="fecha"  format="dd/MM/yyyy hh:mm:ss" /></td>
							</tr>
						</s:iterator>
					</tbody>
				</table>
			</s:else>
			<s:a action="login-success">
				<s:text name="view.back" />
			</s:a>
		</article>
	</section>
	<footer>
		<strong><s:text name="index.footer.part1" /></strong><br /> <em><s:text
				name="index.footer.part2" /> </em>
	</footer>
</body>
