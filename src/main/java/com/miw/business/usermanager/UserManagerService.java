package com.miw.business.usermanager;

import java.util.List;

import com.miw.model.User;

public interface UserManagerService {

	List<User> getUsers() throws Exception;

	User getUserByUsername(String username) throws Exception;

	User newUser(User user) throws Exception;
}
