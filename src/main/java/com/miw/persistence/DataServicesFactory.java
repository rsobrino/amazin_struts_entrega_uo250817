package com.miw.persistence;

import com.miw.persistence.book.BookDataService;
import com.miw.persistence.order.OrderDataService;
import com.miw.persistence.user.UserDataService;
import com.miw.persistence.vat.VATDataService;

public interface DataServicesFactory {
	BookDataService getBookDataService();

	VATDataService getVATDataService();

	UserDataService getUserDataService();

	OrderDataService getOrderDataService();
}
