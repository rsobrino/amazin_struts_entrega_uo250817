package com.miw.presentation.actions;

import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.interceptor.SessionAware;

import com.miw.model.LoginInfo;
import com.miw.model.Order;
import com.miw.model.ShoppingCart;
import com.miw.presentation.order.OrderManagerServiceHelper;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.validator.annotations.RequiredStringValidator;
import com.opensymphony.xwork2.validator.annotations.StringLengthFieldValidator;
import com.opensymphony.xwork2.validator.annotations.Validations;
import com.opensymphony.xwork2.validator.annotations.ValidatorType;

@Result(name = "success", location = "login-success.jsp")

@Validations(stringLengthFields = {
		@StringLengthFieldValidator(type = ValidatorType.SIMPLE, fieldName = "order.payment", minLength = "5", maxLength = "100", trim = true, key = "payment.validation"),
		@StringLengthFieldValidator(type = ValidatorType.SIMPLE, fieldName = "order.address", minLength = "5", maxLength = "100", trim = true, key = "address.validation"),

}, requiredStrings = {
		@RequiredStringValidator(type = ValidatorType.SIMPLE, fieldName = "order.payment", key = "payment.required"),
		@RequiredStringValidator(type = ValidatorType.SIMPLE, fieldName = "order.address", key = "address.required"), })

public class AddOrderAction extends ActionSupport implements SessionAware {

	private static final long serialVersionUID = -4752542581534740735L;
	private Logger logger = Logger.getLogger(this.getClass());
	private Map<String, Object> session = null;

	private Order order;

	@Override
	public String execute() throws Exception {
		logger.debug("Executing " + this.getClass().getName());
		if (session.containsKey("loginInfo")) {

			OrderManagerServiceHelper helper = new OrderManagerServiceHelper();

			LoginInfo loginInfo = (LoginInfo) session.get("loginInfo");

			ShoppingCart shoppingCart = (ShoppingCart) session.get("shoppingcart");
			Order orderSession = (Order) session.get("orderSession");

			if (loginInfo != null || shoppingCart != null || orderSession != null) {

				orderSession.setPayment(order.getPayment());
				orderSession.setAddress(order.getAddress());

				try {
					helper.newOrder(orderSession);

					session.remove("shoppingcart");
					session.remove("orderSession");
					return SUCCESS;

				} catch (Exception ex) {

					logger.error("Error " + this.getClass().getName(), ex);
					return ERROR;
				}

			} else {
				logger.error("Error " + this.getClass().getName());
				return ERROR;
			}

		} else {
			return ERROR;
		}
	}

	@Override
	public void setSession(Map<String, Object> session) {
		this.session = session;
	}

	public Order getOrder() {
		return order;
	}

	public void setOrder(Order order) {
		this.order = order;
	}

	@Override
	public void validate() {
		logger.debug("Validating Order info");

	}

}
