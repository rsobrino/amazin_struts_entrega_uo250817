package com.miw.persistence.order;

import java.util.List;

import javax.persistence.EntityManager;

import org.apache.log4j.Logger;

import com.miw.model.Order;
import com.miw.persistence.Dba;

public class OrderDAO implements OrderDataService {

	protected Logger logger = Logger.getLogger(getClass());

	@Override
	public List<Order> getOrdersByUsername(String username) throws Exception {

		List<Order> resultList = null;
		Dba dba = new Dba();
		try {
			EntityManager em = dba.getActiveEm();

			resultList = em.createQuery("SELECT v FROM Order v WHERE v.username = ?", Order.class)
					.setParameter(1, username).getResultList();

			logger.debug("Get Orders");

		} finally {
			// 100% sure that the transaction and entity manager will be closed
			dba.closeEm();
		}

		// We return the result
		return resultList;
	}

	@Override
	public Order newOrder(Order order) throws Exception {
		Dba dba = new Dba();
		try {
			EntityManager em = dba.getActiveEm();
			em.persist(order);
			em.getTransaction().commit();

			logger.debug("Alta Order: " + order.toString());

		} finally {
			// 100% sure that the transaction and entity manager will be closed
			dba.closeEm();
		}

		// We return the result
		return order;
	}

}
