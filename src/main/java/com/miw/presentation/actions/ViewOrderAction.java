package com.miw.presentation.actions;

import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.interceptor.RequestAware;
import org.apache.struts2.interceptor.SessionAware;

import com.miw.model.LoginInfo;
import com.miw.presentation.order.OrderManagerServiceHelper;
import com.opensymphony.xwork2.ActionSupport;

@ParentPackage(value = "miw.Amazin")

public class ViewOrderAction extends ActionSupport implements RequestAware, SessionAware {

	private static final long serialVersionUID = 8824911775189134182L;

	private Logger logger = Logger.getLogger(this.getClass());

	private Map<String, Object> request = null;
	private Map<String, Object> session = null;

	@Override
	public String execute() throws Exception {
		logger.debug("Executing " + this.getClass().getName());

		if (session.containsKey("loginInfo")) {
			LoginInfo loginInfo = (LoginInfo) session.get("loginInfo");

			OrderManagerServiceHelper helper = new OrderManagerServiceHelper();
			try {
				request.put("orderList", helper.getOrdersByUsername(loginInfo.getLogin()));
			} catch (Exception e) {
				logger.error("Error " + this.getClass().getName(), e);
				return ERROR;
			}
			return SUCCESS;
		} else {
			logger.error("Error " + this.getClass().getName());
			return ERROR;
		}
	}

	@Override
	public void setRequest(Map<String, Object> request) {
		this.request = request;
	}

	@Override
	public void setSession(Map<String, Object> session) {
		this.session = session;
	}
}