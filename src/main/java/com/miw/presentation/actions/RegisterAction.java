package com.miw.presentation.actions;

import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.interceptor.RequestAware;

import com.miw.model.RegisterInfo;
import com.miw.model.User;
import com.miw.presentation.user.UserManagerServiceHelper;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.validator.annotations.EmailValidator;
import com.opensymphony.xwork2.validator.annotations.RequiredStringValidator;
import com.opensymphony.xwork2.validator.annotations.StringLengthFieldValidator;
import com.opensymphony.xwork2.validator.annotations.Validations;
import com.opensymphony.xwork2.validator.annotations.ValidatorType;

@Result(name = "success", location = "/index.jsp")

@Validations(stringLengthFields = {
		@StringLengthFieldValidator(type = ValidatorType.SIMPLE, fieldName = "info.password", minLength = "5", maxLength = "100", trim = true, key = "password.validation"),
		@StringLengthFieldValidator(type = ValidatorType.SIMPLE, fieldName = "info.passwordRepeat", minLength = "5", maxLength = "100", trim = true, key = "password-repeat.validation"),
		@StringLengthFieldValidator(type = ValidatorType.SIMPLE, fieldName = "info.username", minLength = "5", maxLength = "100", trim = true, key = "username.validation"),

}, requiredStrings = {
		@RequiredStringValidator(type = ValidatorType.SIMPLE, fieldName = "info.username", key = "login.required"),
		@RequiredStringValidator(type = ValidatorType.SIMPLE, fieldName = "info.password", key = "password.required"),
		@RequiredStringValidator(type = ValidatorType.SIMPLE, fieldName = "info.passwordRepeat", key = "password-repeat.required"),
		@RequiredStringValidator(type = ValidatorType.SIMPLE, fieldName = "info.email", key = "email.required") }, emails = {
				@EmailValidator(type = ValidatorType.SIMPLE, fieldName = "info.email", key = "email.invalid") })

public class RegisterAction extends ActionSupport implements RequestAware {

	private static final long serialVersionUID = 4674623943937761755L;
	private Logger logger = Logger.getLogger(this.getClass());

	private Map<String, Object> request = null;

	private RegisterInfo info;

	@Override
	public String execute() throws Exception {
		logger.debug("Executing " + this.getClass().getName());
		UserManagerServiceHelper helper = new UserManagerServiceHelper();
		try {
			helper.newUser(new User(info.getUsername(), info.getPassword(), info.getEmail()));
		} catch (Exception e) {
			logger.error("Error " + this.getClass().getName(), e);

			return ERROR;
		}
		return SUCCESS;
	}

	@Override
	public void setRequest(Map<String, Object> request) {
		this.request = request;
	}

	public RegisterInfo getInfo() {
		return info;
	}

	public void setInfo(RegisterInfo info) {
		this.info = info;
	}

	@Override
	public void validate() {
		logger.debug("Validating user info");
		UserManagerServiceHelper helper = new UserManagerServiceHelper();

		if (!info.getPassword().equals(info.getPasswordRepeat())) {
			addFieldError("user.passwordRepeat", getText("password.notequal"));
		}

		try {
			User userBuscado = helper.getUserByUsername(info.getUsername());
			if (userBuscado != null) {
				addFieldError("user.username", getText("username-exists"));
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
