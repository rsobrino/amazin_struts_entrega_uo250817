package com.miw.persistence;

import org.apache.log4j.Logger;

import com.miw.persistence.book.BookDAO;
import com.miw.persistence.book.BookDataService;
import com.miw.persistence.order.OrderDAO;
import com.miw.persistence.order.OrderDataService;
import com.miw.persistence.user.UserDAO;
import com.miw.persistence.user.UserDataService;
import com.miw.persistence.vat.VATDAO;
import com.miw.persistence.vat.VATDataService;

public class SimpleDataServicesFactory implements DataServicesFactory {

	Logger logger = Logger.getLogger(this.getClass());

	@Override
	public BookDataService getBookDataService() {
		logger.debug("Serving an instance of BookDataService from " + this.getClass().getName());
		return new BookDAO();
	}

	@Override
	public VATDataService getVATDataService() {
		logger.debug("Serving an instance of VATDataService from " + this.getClass().getName());
		return new VATDAO();
	}

	@Override
	public UserDataService getUserDataService() {
		logger.debug("Serving an instance of UserDataService from " + this.getClass().getName());
		return new UserDAO();
	}

	@Override
	public OrderDataService getOrderDataService() {
		logger.debug("Serving an instance of OrderDataService from " + this.getClass().getName());
		return new OrderDAO();
	}
}
