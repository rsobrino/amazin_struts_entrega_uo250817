package com.miw.presentation.order;

import java.util.List;

import com.miw.infrastructure.Factories;
import com.miw.model.LoginInfo;
import com.miw.model.Order;
import com.miw.model.ShoppingCart;

public class OrderManagerServiceHelper {

	public Order newOrder(Order order) throws Exception {
		return (Factories.services.getOrderManagerService()).newOrder(order);
	}

	public List<Order> getOrdersByUsername(String username) throws Exception {
		return (Factories.services.getOrderManagerService()).getOrdersByUsername(username);
	}

	public Order createOrderByShoppingCartAndUserInfo(ShoppingCart shoppingCart, LoginInfo loginInfo) throws Exception {
		return (Factories.services.getOrderManagerService()).createOrderByShoppingCartAndUserInfo(shoppingCart,
				loginInfo);
	}

}
