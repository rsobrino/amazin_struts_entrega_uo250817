package com.miw.persistence.user;

import java.util.List;

import com.miw.model.User;

public interface UserDataService {

	List<User> getUsers() throws Exception;

	User getUserByUsername(String username) throws Exception;

	User newUser(User user) throws Exception;

}
