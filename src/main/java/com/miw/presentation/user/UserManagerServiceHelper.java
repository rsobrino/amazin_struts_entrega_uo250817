package com.miw.presentation.user;

import java.util.List;

import com.miw.infrastructure.Factories;
import com.miw.model.User;

public class UserManagerServiceHelper {

	public List<User> getUsers() throws Exception {
		return (Factories.services.getUserManagerService()).getUsers();
	}

	public User getUserByUsername(String username) throws Exception {
		return (Factories.services.getUserManagerService()).getUserByUsername(username);
	}

	public User newUser(User user) throws Exception {
		return (Factories.services.getUserManagerService()).newUser(user);
	}

}
