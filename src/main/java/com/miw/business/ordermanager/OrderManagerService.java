package com.miw.business.ordermanager;

import java.util.List;

import com.miw.model.LoginInfo;
import com.miw.model.Order;
import com.miw.model.ShoppingCart;

public interface OrderManagerService {

	Order newOrder(Order order) throws Exception;

	List<Order> getOrdersByUsername(String username) throws Exception;

	Order createOrderByShoppingCartAndUserInfo(ShoppingCart shoppingCart, LoginInfo loginInfo) throws Exception;

}
