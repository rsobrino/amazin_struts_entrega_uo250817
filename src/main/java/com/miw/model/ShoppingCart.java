package com.miw.model;

import java.util.HashMap;
import java.util.Map;

public class ShoppingCart {

	private Map<String, Integer> list;

	public ShoppingCart() {
		super();
		this.list = new HashMap<String, Integer>();
	}

	public Map<String, Integer> getList() {
		return list;
	}

	public void add(String element) {
		Integer valor = this.list.get(element);
		if (valor == null) {
			this.list.put(element, 1);
		} else {
			this.list.replace(element, ++valor);
		}

	}
}
