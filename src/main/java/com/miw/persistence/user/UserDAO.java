package com.miw.persistence.user;

import java.util.List;

import javax.persistence.EntityManager;

import org.apache.log4j.Logger;

import com.miw.model.User;
import com.miw.persistence.Dba;

public class UserDAO implements UserDataService {

	protected Logger logger = Logger.getLogger(getClass());

	@Override
	public List<User> getUsers() throws Exception {
		List<User> resultList = null;

		Dba dba = new Dba();
		try {
			EntityManager em = dba.getActiveEm();

			resultList = em.createQuery("Select a From User a", User.class).getResultList();

			logger.debug("Result list: " + resultList.toString());

		} finally {
			// 100% sure that the transaction and entity manager will be closed
			dba.closeEm();
		}
		// We return the result
		return resultList;
	}

	@Override
	public User getUserByUsername(String username) throws Exception {

		List<User> resultList = null;
		User user = null;
		Dba dba = new Dba();
		try {
			EntityManager em = dba.getActiveEm();

			resultList = em.createQuery("SELECT v FROM User v WHERE v.username = ?", User.class)
					.setParameter(1, username).getResultList();

			if (resultList.isEmpty()) {
				user = null;
				logger.debug("Get User: ninigun resultado ");

			} else {
				user = resultList.get(0);
				logger.debug("Get User: " + resultList.get(0).toString());
			}

		} finally {
			// 100% sure that the transaction and entity manager will be closed
			dba.closeEm();
		}

		// We return the result
		return user;
	}

	@Override
	public User newUser(User user) throws Exception {
		Dba dba = new Dba();
		try {
			EntityManager em = dba.getActiveEm();
			em.persist(user);
			em.getTransaction().commit();

			logger.debug("Alta usuario: " + user.toString());

		} finally {
			// 100% sure that the transaction and entity manager will be closed
			dba.closeEm();
		}

		// We return the result
		return user;
	}

}
