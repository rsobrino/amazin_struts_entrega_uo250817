<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html >
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<head>
<title><s:text name="title"/></title>
<link rel="stylesheet" href="css/style.css" />
</head>
<body>
	<header>
		<h1 class="header"><s:text name="index.h1"/></h1>
		<h2 class="centered">
			<s:text name="index.h2.part1"/> <em><s:text name="index.h2.part2"/></em> <s:text name="index.h2.part3"/>
		</h2>
	</header>
	<nav>
		<ul>
			<li><a href="#"><s:text name="index.nav.list1"/></a></li>
			<li><a href="http://miw.uniovi.es"><s:text name="index.nav.list2"/></a></li>
			<li><a href="mailto:dd@email.com"><s:text name="index.nav.list3"/></a></li>
		</ul>
	</nav>
	<section>
		<article id="a01">
			<label class="mytitle"><s:text name="login.choose"/></label><br /> 
			<a href="show-books.action"><s:text name="login.catalog"/></a><br /> 
			<a href="show-special-offer.action"><s:text name="login.special"/></a> <br /> 
			<a href="view-order.action"><s:text name="order.showorders"/></a> 
		</article>
	</section>
	<footer>
		<strong><s:text name="index.footer.part1"/></strong><br /> <em><s:text name="index.footer.part2"/> </em>
		<br/> <s:text name="login.visits"/>	<s:property value="%{#application.counter}" />
			
	</footer>
</body>
