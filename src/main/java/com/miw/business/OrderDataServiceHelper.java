package com.miw.business;

import java.util.List;

import com.miw.infrastructure.Factories;
import com.miw.model.Order;

public class OrderDataServiceHelper {

	public Order newOrder(Order order) throws Exception {
		return (Factories.dataServices.getOrderDataService()).newOrder(order);
	}

	public List<Order> getOrdersByUsername(String username) throws Exception {
		return (Factories.dataServices.getOrderDataService()).getOrdersByUsername(username);
	}

}
