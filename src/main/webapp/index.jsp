<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html >
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<head>
<title><s:text name="title"/></title>
<link rel="stylesheet" href="css/style.css" />
</head>
<body>
	<header>
		<h1 class="header"><s:text name="index.h1"/></h1>
		<h2 class="centered">
			<s:text name="index.h2.part1"/> <em><s:text name="index.h2.part2"/></em> <s:text name="index.h2.part3"/>
		</h2>
	</header>
	<nav>
		<ul>
			<li><a href="#"><s:text name="index.nav.list1"/></a></li>
			<li><a href="http://miw.uniovi.es"><s:text name="index.nav.list2"/></a></li>
			<li><a href="mailto:dd@email.com"><s:text name="index.nav.list3"/></a></li>
		</ul>
	</nav>
	<section>
		<article>
			<label class="mytitle"><s:text name="index.article.title"/></label><br />
			<div style="color: red;">
				<s:property value="#request.mymessage" />
				<br />
			</div>
			<s:form action="login"  >
				<s:textfield name="loginInfo.login" key="index.login.label" />
				<s:password name="loginInfo.password" key="index.password.label" />
				<s:textfield name="loginInfo.captcha" key="index.captcha.label" />
				<s:submit />
			</s:form>
			<label class="mytitle"><s:text name="index.create-account"/></label>
			<s:a action="register-form"><s:text name="index.here"/></s:a>

		</article>
	</section>
	<footer>

		<strong><s:text name="index.footer.part1"/></strong><br /> <em><s:text name="index.footer.part2"/> </em>
	</footer>
</body>
