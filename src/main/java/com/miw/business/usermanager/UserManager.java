package com.miw.business.usermanager;

import java.util.List;

import org.apache.log4j.Logger;

import com.miw.business.UserDataServiceHelper;
import com.miw.model.User;

public class UserManager implements UserManagerService {

	@Override
	public List<User> getUsers() throws Exception {

		Logger logger = Logger.getLogger(this.getClass());

		logger.debug("Asking for users");

		List<User> users = (new UserDataServiceHelper()).getUsers();
		return users;
	}

	@Override
	public User getUserByUsername(String username) throws Exception {
		Logger logger = Logger.getLogger(this.getClass());

		logger.debug("Asking for users by username");

		User user = (new UserDataServiceHelper()).getUserByUsername(username);
		return user;
	}

	@Override
	public User newUser(User user) throws Exception {
		Logger logger = Logger.getLogger(this.getClass());

		logger.debug("Creating new user");

		return (new UserDataServiceHelper()).newUser(user);

	}

}
