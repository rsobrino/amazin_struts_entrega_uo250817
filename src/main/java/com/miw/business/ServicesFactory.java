package com.miw.business;

import com.miw.business.bookmanager.BookManagerService;
import com.miw.business.ordermanager.OrderManagerService;
import com.miw.business.usermanager.UserManagerService;

public interface ServicesFactory {
	public BookManagerService getBookManagerService();

	UserManagerService getUserManagerService();

	OrderManagerService getOrderManagerService();

}
