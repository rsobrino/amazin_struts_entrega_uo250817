package com.miw.presentation.actions;

import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.SessionAware;

import com.miw.model.LoginInfo;
import com.miw.model.ShoppingCart;
import com.miw.presentation.order.OrderManagerServiceHelper;
import com.opensymphony.xwork2.ActionSupport;

public class AddOrderFormAction extends ActionSupport implements SessionAware {

	private static final long serialVersionUID = -4752542581534740735L;
	private Logger logger = Logger.getLogger(this.getClass());
	private Map<String, Object> session = null;

	@Override
	public String execute() throws Exception {
		logger.debug("Executing " + this.getClass().getName());
		if (session.containsKey("loginInfo")) {

			OrderManagerServiceHelper helper = new OrderManagerServiceHelper();

			LoginInfo loginInfo = (LoginInfo) session.get("loginInfo");

			ShoppingCart shoppingCart = (ShoppingCart) session.get("shoppingcart");

			session.put("orderSession", helper.createOrderByShoppingCartAndUserInfo(shoppingCart, loginInfo));

			return SUCCESS;
		} else {
			return ERROR;
		}
	}

	@Override
	public void setSession(Map<String, Object> session) {
		this.session = session;
	}

}
