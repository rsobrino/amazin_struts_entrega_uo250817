package com.miw.business;

import org.apache.log4j.Logger;

import com.miw.business.bookmanager.BookManager;
import com.miw.business.bookmanager.BookManagerService;
import com.miw.business.ordermanager.OrderManager;
import com.miw.business.ordermanager.OrderManagerService;
import com.miw.business.usermanager.UserManager;
import com.miw.business.usermanager.UserManagerService;

public class SimpleServicesFactory implements ServicesFactory {

	Logger logger = Logger.getLogger(this.getClass());

	@Override
	public BookManagerService getBookManagerService() {
		logger.debug("Serving an instance of BookManagerService from " + this.getClass().getName());
		return new BookManager();
	}

	@Override
	public UserManagerService getUserManagerService() {
		logger.debug("Serving an instance of UserManagerService from " + this.getClass().getName());
		return new UserManager();
	}

	@Override
	public OrderManagerService getOrderManagerService() {
		logger.debug("Serving an instance of OrderManagerService from " + this.getClass().getName());
		return new OrderManager();
	}

}
