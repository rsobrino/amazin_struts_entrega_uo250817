<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html >
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<head>
<title><s:text name="title" /></title>
<link rel="stylesheet" href="css/style.css" />
</head>
<body>
	<header>
		<h1 class="header">
			<s:text name="index.h1" />
		</h1>
		<h2 class="centered">
			<s:text name="index.h2.part1" />
			<em><s:text name="index.h2.part2" /></em>
			<s:text name="index.h2.part3" />
		</h2>
	</header>
	<nav>
		<ul>
			<li><a href="#"><s:text name="index.nav.list1" /></a></li>
			<li><a href="http://miw.uniovi.es"><s:text
						name="index.nav.list2" /></a></li>
			<li><a href="mailto:dd@email.com"><s:text
						name="index.nav.list3" /></a></li>
		</ul>
	</nav>
	<section>
		<article>
			<s:if test="%{#session.shoppingcart == null}">

				<p>
					<s:text name="view.nocart" />
				</p>
				<s:a action="show-books">
					<s:text name="view.back" />
				</s:a>
			</s:if>
			<s:else>
				<table>
					<caption>
						<s:text name="view.cart" />
					</caption>
					<thead>
						<tr>
							<th><s:text name="view.title" /></th>
							<th><s:text name="view.quantity" /></th>
						</tr>
					</thead>
					<tbody>
						<s:iterator value="%{#session.shoppingcart.list}">
							<tr>
								<td><s:property value="key" /></td>
								<td><s:property value="value" /></td>
							</tr>
						</s:iterator>
					</tbody>
				</table>				
				<s:a action="show-books">
					<s:text name="view.back" />
				</s:a>				
				<s:a action="add-order-form">
					<s:text name="view.addorder" />
				</s:a>
			</s:else>
		</article>
	</section>
	<footer>
		<strong><s:text name="index.footer.part1" /></strong><br /> <em><s:text
				name="index.footer.part2" /> </em>
	</footer>
</body>


