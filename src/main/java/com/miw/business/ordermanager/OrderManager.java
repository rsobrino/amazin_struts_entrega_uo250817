package com.miw.business.ordermanager;

import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.apache.log4j.Logger;

import com.miw.business.OrderDataServiceHelper;
import com.miw.model.Book;
import com.miw.model.LoginInfo;
import com.miw.model.Order;
import com.miw.model.ShoppingCart;
import com.miw.presentation.book.BookManagerServiceHelper;

public class OrderManager implements OrderManagerService {

	@Override
	public Order newOrder(Order order) throws Exception {
		Logger logger = Logger.getLogger(this.getClass());

		logger.debug("Creating new order");

		return (new OrderDataServiceHelper()).newOrder(order);

	}

	@Override
	public List<Order> getOrdersByUsername(String username) throws Exception {
		Logger logger = Logger.getLogger(this.getClass());

		logger.debug("Asking for orders by username");

		return (new OrderDataServiceHelper()).getOrdersByUsername(username);
	}

	@Override
	public Order createOrderByShoppingCartAndUserInfo(ShoppingCart shoppingCart, LoginInfo loginInfo) throws Exception {

		int numeroLibros = 0;
		double total = 0.0;
		Object[] keys = shoppingCart.getList().keySet().toArray();
		for (int i = 0; i < keys.length; i++) {
			Book book = (new BookManagerServiceHelper()).getBookByTitle((String) keys[i]);
			numeroLibros += shoppingCart.getList().get(keys[i]);
			total += book.getPrice() * shoppingCart.getList().get(keys[i]);
		}

		Order order = new Order();
		order.setCode(UUID.randomUUID().toString().substring(0, 7));
		order.setUsername(loginInfo.getLogin());
		order.setNumeroLibros(numeroLibros);
		order.setTotal(redondearDecimales(total, 2));
		order.setFecha(Date.from(Instant.now()));

		return order;
	}

	public static double redondearDecimales(double valorInicial, int numeroDecimales) {
		double parteEntera, resultado;
		resultado = valorInicial;
		parteEntera = Math.floor(resultado);
		resultado = (resultado - parteEntera) * Math.pow(10, numeroDecimales);
		resultado = Math.round(resultado);
		resultado = (resultado / Math.pow(10, numeroDecimales)) + parteEntera;
		return resultado;
	}

}
